package com.campus.controller;

import com.campus.entity.Lost;
import com.campus.service.LostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/lost")
public class LostController {

    @Autowired
    private LostService lostService;

    /**
     * 查询所有丢失物品
     * @return
     */
    @RequestMapping(value = "/queryLost", method = RequestMethod.POST)
    public Map<String, Object> queryLost(String page){
        Map<String, Object> modelMap = new HashMap<>();
        try {
            System.out.println(page);
            modelMap.put("queryLost",lostService.queryLost(Integer.parseInt(page)*15));
            modelMap.put("state","true");
        }catch (Exception e){
            modelMap.put("state","false");
        }
        return modelMap;
    }

    @RequestMapping(value = "/queryLostById", method = RequestMethod.POST)
    public Map<String, Object> queryLostById(Lost lost){
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("lost",lostService.queryLostById(lost));
        return modelMap;
    }
    @RequestMapping(value = "/queryLostByUserId", method = RequestMethod.POST)
    public Map<String, Object> queryLostByUserId(String userId){
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("lost",lostService.queryLostByUserId(userId));
        return modelMap;
    }
    @RequestMapping(value = "/updateLostUser", method = RequestMethod.POST)
    public Map<String, Object> updateLostUser(Lost lost){
        Map<String, Object> modelMap = new HashMap<>();
        try {
            int i=lostService.updateLostUser(lost);
            if(i>0)
                modelMap.put("state","true");
            else
                modelMap.put("state","false");
        }catch (Exception e){
            modelMap.put("state","false");
        }
        return modelMap;
    }
    @RequestMapping(value = "/addLost", method = RequestMethod.POST)
    public Map<String, Object> addLost(Lost lost){
        Map<String, Object> modelMap = new HashMap<>();
        try {
            int i=lostService.addLost(lost);
            if(i>0)
                modelMap.put("state","true");
            else
                modelMap.put("state","false");
        }catch (Exception e){
            modelMap.put("state","false");
        }
        return modelMap;
    }
    @RequestMapping(value = "/updateGetFlag", method = RequestMethod.POST)
    public Map<String, Object> updateGetFlag(String lostId){
        Map<String, Object> modelMap = new HashMap<>();
        try {
            int i=lostService.updateGetFlag(Integer.parseInt(lostId));
            if(i>0)
                modelMap.put("state","true");
            else
                modelMap.put("state","false");
        }catch (Exception e){
            modelMap.put("state","false");
        }
        return modelMap;
    }
}
