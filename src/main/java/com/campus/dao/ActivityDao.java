package com.campus.dao;

import com.campus.entity.Activity;

import java.util.Date;
import java.util.List;

/*
 * 接口名   :ActivityDao
 * 描  述   :活动举办数据访问对象
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-28
 * 说  明   :mybatis绑定
 */
public interface ActivityDao {

    //添加活动
    int addActivity(Activity activity);

    //删除活动
    int delActivity(Activity activity);

    //删除报名
    int delEnRoll(Activity activity);

    //查看活动
    Activity getActivity(int activityId);

    //修改活动
    int setActivity(Activity activity);

    //罗列未过期的所有活动
    List<Activity> queryActivity(Date currentdate);

    //罗列用户活动
    List<Activity> queryUserActivity(String userId);

    //查看活动数量
    int getCount(String userId);

    List<Activity> searchActivity(String key);
}
