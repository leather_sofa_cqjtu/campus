package com.campus.service;

import com.campus.entity.Answer;
import com.campus.entity.Comment;
import com.campus.entity.Problem;
import com.campus.entity.User;

import java.util.List;

/*
 * 接口名   :CommentService
 * 描  述   :问答社区的回答接口
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-19
 * 说  明   :
 */
public interface CommentService {
    boolean addComment(Comment comment);//发布评论
    boolean delComment(Comment comment);//删除评论
    List<Comment> queryComment(Answer answer);//罗列评论
    List<Comment> queryUserComment(User user);//罗列评论
    int getCount(Answer answer);//返回评论数量
}
