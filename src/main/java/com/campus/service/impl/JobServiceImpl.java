package com.campus.service.impl;

import com.campus.dao.JobDao;
import com.campus.entity.Job;
import com.campus.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class JobServiceImpl implements JobService {

    @Autowired
    private JobDao jobDao;

    @Override
    public List<Job> queryJob(){
        return jobDao.queryJob();
    }
    @Override
    public int addJob(Job job){
        return jobDao.addJob(job);
    }
}
