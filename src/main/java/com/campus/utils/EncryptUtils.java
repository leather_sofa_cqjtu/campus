package com.campus.utils;

import org.apache.commons.codec.digest.DigestUtils;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.IOException;

//加密工具类
public class EncryptUtils {


    //Base64加密
    public static String encodeBase64(String input) {
        return new BASE64Encoder().encodeBuffer(input.getBytes());
    }

    //Base64解密
    public static String decodeBase64(String input) throws IOException {
        byte[] bytes = new BASE64Decoder().decodeBuffer(input);
        return new String(bytes);
    }

    public static String md5(String input) {
        String salt = "helloworld";
        return DigestUtils.md5Hex(input + salt);
    }

    public static String sha1(String input) {
        return DigestUtils.sha1Hex(input.getBytes());
    }

}
