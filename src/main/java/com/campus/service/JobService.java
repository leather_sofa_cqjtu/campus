package com.campus.service;

import com.campus.entity.Job;

import java.util.List;

public interface JobService {
    List<Job> queryJob();
    int addJob(Job job);
}
