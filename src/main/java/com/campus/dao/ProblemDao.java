package com.campus.dao;

import com.campus.entity.Problem;
import com.campus.entity.User;

import java.util.List;

/*
 * 接口名   :ProblemDao
 * 描  述   :问答社区的问题数据访问对象
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-19
 * 说  明   :mybatis绑定
 */
public interface ProblemDao{
    //发布问题
    int addProblem(Problem problem);

    //删除评论
    int delComment(Problem problem);

    //删除回答
    int delAnswer(Problem problem);

    //删除问题
    int delProblem(Problem problem);

    //查看当前问题
    Problem getProblem(int problemId);

    //罗列问题
    List<Problem> queryProblem();

    //罗列当前用户的问题
    List<Problem> queryUserProblem(String userId);

    //返回问题数量
    int getCount(String userId);
}
