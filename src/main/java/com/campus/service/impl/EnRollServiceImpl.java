package com.campus.service.impl;


import com.campus.dao.ActivityDao;
import com.campus.dao.EnRollDao;
import com.campus.entity.Activity;
import com.campus.entity.EnRoll;
import com.campus.entity.User;
import com.campus.service.EnRollService;

import com.campus.utils.StringsUtils;
import com.mysql.jdbc.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Date;
import java.util.List;




/*
 * 类  名   :EnRollServiceImpl
 * 描  述   :活动报名逻辑处理类
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-29
 * 说  明   :逻辑处理
 */

@Service
public class EnRollServiceImpl implements EnRollService {

    @Autowired
    private EnRollDao enRollDao;
    @Autowired
    private ActivityDao activityDao;

    //添加报名-事务处理
    @Transactional
    @Override
    public boolean addEnRoll(EnRoll enRoll) {
        try {
            if (StringUtils.isNullOrEmpty(enRoll.getUser().getUserId())) {
                throw new RuntimeException("userId不存在|删除失败");
            }
            Activity activity = activityDao.getActivity(enRoll.getActivity().getActivityId());
            int nowPeople = activity.getNowPeople();
            if (nowPeople >= activity.getMaxPeople()) {
                return false;
            }
            nowPeople += 1;
            activity.setNowPeople(nowPeople);
            enRoll.setCreateTime(new Date());
            int result = enRollDao.addEnRoll(enRoll);
            result += activityDao.setActivity(activity);
            return result == 2;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public boolean delEnRoll(EnRoll enRoll) {
        try {
            if (StringsUtils.isNullOrEmpty(enRoll.getUser().getUserId())) {
                throw new RuntimeException("userId不存在|删除失败");
            }
            Activity activity = activityDao.getActivity(enRoll.getActivity().getActivityId());
            int nowPeople = activity.getNowPeople();
            if (nowPeople >= activity.getMaxPeople()) {
                return false;
            }
            nowPeople -= 1;
            activity.setNowPeople(nowPeople);
            int result = enRollDao.delEnRoll(enRoll);
            result += activityDao.setActivity(activity);
            return result == 2;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public EnRoll getEnRoll(EnRoll enRoll) {
        return null;
    }

    @Override
    public List<EnRoll> queryEnRoll(Activity activity) {
        return enRollDao.queryEnRoll(activity.getActivityId());
    }

    @Override
    public List<EnRoll> queryUserEnRoll(User user) {
        if (StringsUtils.isNullOrEmpty(user.getUserId())) {
            return null;
        }
        return enRollDao.queryUserEnRoll(user.getUserId());
    }

    @Override
    public int getCount(User user) {
        if (StringsUtils.isNullOrEmpty(user.getUserId())) {
            return 0;
        }
        return enRollDao.getCount(user.getUserId());
    }

    @Override
    public boolean isExist(int activityId, String userId) {
        System.out.println(activityId);
        return enRollDao.isExist(activityId, userId) != null;
    }
}
