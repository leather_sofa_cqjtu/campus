package com.campus.dao;

import com.campus.entity.Answer;
import com.campus.entity.Comment;
import com.campus.entity.User;
import java.util.List;

/*
 * 接口名   :CommnentDao
 * 描  述   :问答社区的评论数据访问对象
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-20
 * 说  明   :mybatis绑定
 */

public interface CommentDao {
    //添加评论
    int addComment(Comment comment);

    //删除评论
    int delComment(Comment comment);

    //罗列评论
    List<Comment> queryComment(int answerId);

    //罗列用户评论
    List<Comment> queryUserComment(String userId);

    //查询条数
    int getCount(int answerId);
}
