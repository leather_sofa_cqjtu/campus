package com.campus.controller;

import com.campus.entity.Activity;
import com.campus.entity.User;
import com.campus.service.ActivityService;
import com.campus.service.EnRollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("/user")
public class ActivityController {
    @Autowired
    private ActivityService activityService;

    @Autowired
    private EnRollService enRollService;

    //添加活动
    @RequestMapping(value = "/addActivity", method = RequestMethod.POST)
    public Map<String, Object> addActivity(Activity activity) {
        Map<String, Object> modelMap = new HashMap<>();
        System.out.println(activity.getMaxPeople());
        try {
            if (activityService.addActivity(activity)) {
                modelMap.put("status", true);
                modelMap.put("errMsg", "发布活动成功");
            } else {
                modelMap.put("status", false);
                modelMap.put("errMsg", "发布失败,可能网络出小差了");
            }
        } catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }
        return modelMap;
    }

    //删除活动
    @RequestMapping(value = "/delActivity", method = RequestMethod.POST)
    public Map<String, Object> delActivity(Activity activity) {
        Map<String, Object> modelMap = new HashMap<>();
        try {
            if (activityService.delActivity(activity)) {
                modelMap.put("status", true);
                modelMap.put("errMsg", "删除成功");
            } else {
                modelMap.put("status", false);
                modelMap.put("errMsg", "删除失败,可能网络出小差了");
            }
        } catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }
        return modelMap;
    }

    //得到活动
    @RequestMapping(value = "/getActivity", method = RequestMethod.POST)
    public Map<String, Object> setActivity(int activityId,String userId) {
        Map<String, Object> modelMap = new HashMap<>();
        boolean isSc = false;
        if(enRollService.isExist(activityId,userId)){
            isSc = true;
        }
        modelMap.put("activity",activityService.getActivity(activityId));
        modelMap.put("isSC",isSc);
        return modelMap;
    }


    //修改活动
    @RequestMapping(value = "/setActivity", method = RequestMethod.POST)
    public Map<String, Object> setActivity(Activity activity) {
        Map<String, Object> modelMap = new HashMap<>();
        try {
            if (activityService.setActivity(activity)) {
                modelMap.put("status", true);
                modelMap.put("errMsg", "修改成功");
            } else {
                modelMap.put("status", false);
                modelMap.put("errMsg", "修改失败,可能网络出小差了");
            }
        } catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }
        return modelMap;
    }

    //罗列未过期的活动
    @RequestMapping(value = "/queryActivity", method = RequestMethod.POST)
    public Map<String, Object> queryActivity() {
        Map<String, Object> modelMap = new HashMap<>();

        List<Activity> activities = activityService.queryActivity();

        List<Activity> latest = new ArrayList<>();
        for (int i=0;i<4&&i<activities.size();i++){
            latest.add(activities.get(i));
        }
        Collections.sort(activities, Comparator.comparingInt(Activity::getNowPeople));
        List<Activity> most = activities.subList(0, activities.size() > 4 ? 4 : activities.size());
        modelMap.put("most", most);
        modelMap.put("latest", latest);
        return modelMap;
    }

    //罗列用户的活动
    @RequestMapping(value = "/queryUserActivity", method = RequestMethod.POST)
    public Map<String, Object> queryUserActivity(User user) {
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("activityUserList", activityService.queryUserActivity(user));
        return modelMap;
    }

    //返回用户活动数量
    @RequestMapping(value = "/getUserActivityCount", method = RequestMethod.POST)
    public Map<String, Object> getUserActivityCount(User user) {
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("count", activityService.getCount(user));
        return modelMap;
    }

    //返回用户活动数量
    @RequestMapping(value = "/searchactivity", method = RequestMethod.POST)
    public Map<String, Object> getUserActivityCount(String key) {
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("results", activityService.searchActivity(key));
        return modelMap;
    }

}
