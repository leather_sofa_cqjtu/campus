package com.campus.dao;

import com.campus.entity.Job;

import java.util.List;

public interface JobDao {
    List<Job> queryJob();
    int addJob(Job job);
}
