package com.campus.controller;

import com.campus.entity.Problem;
import com.campus.entity.User;
import com.campus.service.ProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/*
 * 类  名   :ProblemController
 * 描  述   :问答社区的问题接口
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-21
 * 说  明   :前后端绑定
 */
@RestController
@RequestMapping("/user")
public class ProblemController {

    @Autowired
    private ProblemService problemService;

    //添加问题
    @RequestMapping(value = "/addProblem",method = RequestMethod.POST)
    public Map<String,Object> addProblem(Problem problem){
        Map<String, Object> modelMap =new HashMap<>();
        try {
            if(problemService.addProblem(problem)){
                modelMap.put("status",true);
                modelMap.put("errMsg","添加问题成功");
            }
            else{
                modelMap.put("status", false);
                modelMap.put("errMsg", "添加问题失败,可能网络出小差了");
            }
        }catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }
        return modelMap;
    }

    //删除问题
    @RequestMapping(value = "/delProblem",method = RequestMethod.POST)
    public Map<String,Object> delProblem(Problem problem){
        Map<String,Object> modelMap = new HashMap<>();
        try {
            if(problemService.delProblem(problem)){
                modelMap.put("status",true);
                modelMap.put("errMsg","删除问题成功");
            }else{
                modelMap.put("status",false);
                modelMap.put("errMsg","删除问题失败，可能网络出小差了");
            }
        }catch (Exception e){
            modelMap.put("status",false);
            modelMap.put("errMsg",e.getMessage());
        }
        return modelMap;
    }
    //返回所有问题列表
    @RequestMapping(value = "/queryProblem",method = RequestMethod.POST)
    public Map<String,Object> queryProblem(){
        Map<String,Object>modelMap = new HashMap<>();
        modelMap.put("problemList",problemService.queryProblem());
        return modelMap;
    }

    //返回当前用户的问题列表
    @RequestMapping(value = "queryUserProblem",method = RequestMethod.POST)
    public Map<String,Object> queryUserProblem(User user){
        Map<String,Object>modelMap = new HashMap<>();
        modelMap.put("problemUserList",problemService.queryUserProblem(user));
        return modelMap;
    }

    //查看问题数量
    @RequestMapping(value = "getProblemCount",method = RequestMethod.POST)
    public Map<String,Object> getCommentCount(User user){
        Map<String,Object>modelMap = new HashMap<>();
        modelMap.put("count",problemService.getCount(user));
        return modelMap;
    }
}
