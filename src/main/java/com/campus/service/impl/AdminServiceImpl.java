package com.campus.service.impl;

import com.campus.dao.AdminDao;
import com.campus.entity.Admin;
import com.campus.service.AdminService;
import com.campus.utils.JwtUtil;
import com.campus.utils.StringsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceImpl implements AdminService {


    @Autowired
    private AdminDao adminDao;

    //返回一个token
    @Override
    public String login(Admin admin) {
       if(null==admin||StringsUtils.isNullOrEmpty(admin.getAdminName())||StringsUtils.isNullOrEmpty(admin.getPassword())){
           throw  new RuntimeException("参数为空");
       }
       Admin login = adminDao.login(admin);
       if(null == login){
           throw  new RuntimeException("登录失败,账号或者密码错误!");
       }
       return JwtUtil.sign(login, 30L * 24L * 3600L * 1000L);
    }

    @Override
    public boolean isExist(Admin admin) {
        return adminDao.login(admin) != null;
    }
}
