package com.campus.controller;


import com.campus.entity.Answer;
import com.campus.entity.Comment;
import com.campus.entity.User;
import com.campus.service.AnswerService;
import com.campus.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/*
 * 类  名   :CommentController
 * 描  述   :问答社区的评论接口
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-21
 * 说  明   :前后端绑定
 */
@RestController
@RequestMapping("/user")
public class CommentController {

    @Autowired
    private CommentService commentService;
    @Autowired
    private AnswerService answerService;

    //添加评论
    @RequestMapping(value = "/addComment",method = RequestMethod.POST)
    public Map<String,Object> addComment(Comment comment){
        Map<String, Object> modelMap = new HashMap<>();
        try{
            if(commentService.addComment(comment)){
                modelMap.put("status", true);
                modelMap.put("errMsg","评论成功");
            }else {
                modelMap.put("status", false);
                modelMap.put("errMsg", "评论失败,可能网络出小差了");
            }
        }catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }
        return modelMap;
    }
    //删除评论
    @RequestMapping(value = "/delComment",method = RequestMethod.POST)
    public Map<String,Object> delComment(Comment comment){
        Map<String,Object> modelMap = new HashMap<>();
        try {
            if(commentService.delComment(comment)){
                modelMap.put("status",true);
                modelMap.put("errMsg","删除评论成功");
            }else{
                modelMap.put("status",false);
                modelMap.put("errMsg","删除评论失败，可能网络出小差了");
            }
        }catch (Exception e){
            modelMap.put("status",false);
            modelMap.put("errMsg",e.getMessage());
        }
        return modelMap;
    }
    //返回该回答下的所有评论
    @RequestMapping(value = "/queryComment",method = RequestMethod.POST)
    public Map<String,Object> queryComment(Answer answer){
        Map<String,Object> modelMap = new HashMap<>();
        modelMap.put("answerCurrent",answerService.getAnswer(answer));
        modelMap.put("commentList",commentService.queryComment(answer));
        return modelMap;
    }
    //返回该用户的所有评论
    @RequestMapping(value = "/queryUserComment",method = RequestMethod.POST)
    public  Map<String, Object> queryUserComment(User user){
        Map<String,Object> modelMap = new HashMap<>();
        modelMap.put("commentUserList",commentService.queryUserComment(user));
        return modelMap;
    }
    //查看评论数量
    @RequestMapping(value = "/getCommentcount",method = RequestMethod.POST)
    public Map<String,Object> getCommentCount(Answer answer){
        Map<String,Object>modelMap = new HashMap<>();
        modelMap.put("count",commentService.getCount(answer));
        return modelMap;
    }
}
