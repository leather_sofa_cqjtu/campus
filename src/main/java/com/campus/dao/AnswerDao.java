package com.campus.dao;

import com.campus.entity.Answer;
import com.campus.entity.Problem;
import com.campus.entity.User;

import java.util.List;

/*
 * 接口名   :AnswerDao
 * 描  述   :问答社区的回答数据访问对象
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-19
 * 说  明   :mybatis绑定
 */
public interface AnswerDao{
    //发布解答
    int addAnswer(Answer answer);

    //删除评论
    int delComment(Answer answer);

    //删除解答
    int delAnswer(Answer answer);

    //获取当前解答
    Answer getAnswer(int answerId);

    //罗列解答
    List<Answer> queryAnswer(int problemId);

    //罗列用户解答
    List<Answer> queryUserAnswer(String userId);

    //返回解答数量
    int getCount(int problemId);
}
