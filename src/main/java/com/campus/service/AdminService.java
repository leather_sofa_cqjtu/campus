package com.campus.service;

import com.campus.entity.Admin;

public interface AdminService {

    String login(Admin admin);

    boolean isExist(Admin admin);
}
