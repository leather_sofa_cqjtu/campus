package com.campus.service;

import com.campus.entity.Answer;
import com.campus.entity.Problem;
import com.campus.entity.User;

import java.util.List;

/*
 * 接口名   :AnswerService
 * 描  述   :问答社区的问题接口
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-19
 * 说  明   :
 */
public interface AnswerService {
    boolean addAnswer(Answer answer);//发布解答
    boolean delAnwer(Answer answer);//删除解答
    Answer getAnswer(Answer answer);//返回当前问题
    List<Answer> queryAnswer(Problem problem);//罗列解答
    List<Answer> queryUserAnswer(User user);//罗列解答
    int getCount(Problem problem);//返回解答数量
}
