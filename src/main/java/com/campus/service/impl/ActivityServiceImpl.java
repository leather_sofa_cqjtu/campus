package com.campus.service.impl;

import com.campus.dao.ActivityDao;
import com.campus.entity.Activity;
import com.campus.entity.User;
import com.campus.service.ActivityService;

import com.campus.utils.StringsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/*
 * 类  名   :ActivityServiceImpl
 * 描  述   :活动逻辑处理类
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-29
 * 说  明   :逻辑处理
 */

@Service
public class ActivityServiceImpl implements ActivityService {
    @Autowired
    private ActivityDao activityDao;

    @Override
    public boolean addActivity(Activity activity) {
        try {
            if (StringsUtils.isNullOrEmpty(activity.getUser().getUserId())) {
                throw new RuntimeException("userId不存在");
            }
            activity.setNowPeople(0);
            int result = activityDao.addActivity(activity);
            return result > 0;
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Transactional
    @Override
    public boolean delActivity(Activity activity) {
        try {
            if (StringsUtils.isNullOrEmpty(activity.getUser().getUserId())) {
                return false;
            }
            int result = activityDao.delEnRoll(activity);
            result += activityDao.delActivity(activity);
            return result > 0;
        } catch (Exception e) {
            throw new RuntimeException("userId不存在|删除失败");
        }
    }

    @Override
    public Activity getActivity(int activityId) {
        return activityDao.getActivity(activityId);
    }

    @Transactional
    @Override
    public boolean setActivity(Activity activity) {
        try {
            if (StringsUtils.isNullOrEmpty(activity.getUser().getUserId())) {
                return false;
            }
            int result = activityDao.setActivity(activity);
            return result > 0;
        } catch (Exception e) {
            throw new RuntimeException("userId不存在|修改失败");
        }
    }

    @Override
    public List<Activity> queryActivity() {
        return activityDao.queryActivity(new Date());
    }

    @Override
    public List<Activity> queryUserActivity(User user) {
        if (StringsUtils.isNullOrEmpty(user.getUserId())) {
            return null;
        }
        return activityDao.queryUserActivity(user.getUserId());
    }

    @Override
    public int getCount(User user) {
        if (StringsUtils.isNullOrEmpty(user.getUserId())) {
            return 0;
        }
        return activityDao.getCount(user.getUserId());
    }

    @Override
    public List<Activity> searchActivity(String key) {
        return activityDao.searchActivity(key);
    }
}
