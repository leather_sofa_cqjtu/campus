package com.campus.service.impl;

import com.campus.dao.CommentDao;
import com.campus.entity.Answer;
import com.campus.entity.Comment;
import com.campus.entity.User;
import com.campus.service.CommentService;
import com.campus.utils.StringsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/*
 * 类  名   :CommentServiceImpl
 * 描  述   :问答社区的评论服务逻辑处理类
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-21
 * 说  明   :逻辑处理
 */

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentDao commentDao;
    @Override
    public boolean addComment(Comment comment) {
        try {
            if(StringsUtils.isNullOrEmpty(comment.getUser().getUserId())){
                return false;
            }
            comment.setCreateTime(new Date());
            int result = commentDao.addComment(comment);
            return result>0;
        }catch (Exception e) {
            throw  new RuntimeException("userId不存在");
        }
    }
    @Transactional
    @Override
    public boolean delComment(Comment comment) {
        try {
            if(StringsUtils.isNullOrEmpty(comment.getUser().getUserId())){
                return false;
            }
            int result = commentDao.delComment(comment);
            return result>0;
        }catch (Exception e){
            throw new RuntimeException("userId不存在|删除失败");
        }

    }

    @Override
    public List<Comment> queryComment(Answer answer) {
        if(StringsUtils.isNullOrEmpty(answer.getAnswerId().toString())){
            return null;
        }
        return commentDao.queryComment(answer.getAnswerId());
    }

    @Override
    public List<Comment> queryUserComment(User user) {
        if(StringsUtils.isNullOrEmpty(user.getUserId())){
            return null;
        }
        return commentDao.queryUserComment(user.getUserId());
    }

    @Override
    public int getCount(Answer answer) {
       if(StringsUtils.isNullOrEmpty(answer.getAnswerId().toString())){
           return 0;
       }
       return commentDao.getCount(answer.getAnswerId());
    }
}
