package com.campus.service;

import com.campus.entity.Lost;

import java.util.List;

public interface LostService {

    List<Lost> queryLost(int page);
    List<Lost> queryLostByUserId(String userId);
    Lost queryLostById(Lost lost);
    int addLost(Lost lost);
    int updateLostUser(Lost lost);
    int updateGetFlag(int lostId);
}
