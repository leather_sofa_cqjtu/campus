package com.campus.dao;

import com.campus.entity.Lost;

import java.util.List;

public interface LostDao {

    //查询所有丢失物品
    List<Lost> queryLost(int page);
    //根据userId查丢失物品
    List<Lost> queryLostByUserId(String userId);
    //根据lostId查询
    Lost queryLostById(Lost lost);
    //发布新的失物招领
    int addLost(Lost lost);
    //修改getuser
    int updateLostUser(Lost lost);
    //确认领取
    int updateGetFlag(int lostId);
}
