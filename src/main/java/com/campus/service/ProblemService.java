package com.campus.service;

import com.campus.entity.Problem;
import com.campus.entity.User;

import java.util.List;

/*
 * 接口名   :ProblemService
 * 描  述   :问答社区的问题接口
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-19
 * 说  明   :
 */

public interface ProblemService {
    boolean addProblem(Problem problem);//发布问题
    boolean delProblem(Problem problem);//删除问题
    Problem getProblem(Problem problem);//返回当前问题
    List<Problem> queryProblem();//罗列问题
    List<Problem> queryUserProblem(User user);//罗列当前用户的问题
    int getCount(User user);//返回问题数量
}
