package com.campus.service.impl;

import com.campus.dao.AnswerDao;
import com.campus.dao.ProblemDao;
import com.campus.entity.Problem;
import com.campus.entity.User;
import com.campus.service.ProblemService;
import com.campus.utils.StringsUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/*
 * 类  名   :ProblemServiceImpl
 * 描  述   :问答社区的问题服务逻辑处理类
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-21
 * 说  明   :逻辑处理
 */
@Service
public class ProblemServiceImpl implements ProblemService {
    @Autowired
    private ProblemDao problemDao;

    @Autowired
    private AnswerDao answerDao;
    @Override
    public boolean addProblem(Problem problem) {
        try {
            if(StringsUtils.isNullOrEmpty(problem.getUser().getUserId())){
               return false;
            }
            problem.setCreateTime(new Date());
            int result = problemDao.addProblem(problem);
            return result>0;
        }catch (Exception e) {
            throw  new RuntimeException("userId不存在");
        }
    }
    /*
     * 事务处理
     * 处理顺序是
     * 1删除评论
     * 2删除回答
     * 3删除问题
     */
    @Transactional
    @Override
    public boolean delProblem(Problem problem) {
        try {
            if(StringsUtils.isNullOrEmpty(problem.getUser().getUserId())) {
                return false;
            }

            int result = problemDao.delComment(problem);
            result += problemDao.delAnswer(problem);
            result += problemDao.delProblem(problem);
            return result>0;
        }catch (Exception e) {
            throw  new RuntimeException("userId不存在|删除失败");
        }
    }

    @Override
    public Problem getProblem(Problem problem) {
        return problemDao.getProblem(problem.getProblemId());
    }

    @Override
    public List<Problem> queryProblem() {
        List<Problem> temp=problemDao.queryProblem();
        for(int i=0;i<temp.size();i++){
            temp.get(i).setAnswerNum(answerDao.getCount(temp.get(i).getProblemId()));
        }
        return temp;
    }

    @Override
    public List<Problem> queryUserProblem(User user) {
        if(StringsUtils.isNullOrEmpty(user.getUserId())){
            return null;
        }
        List<Problem> temp=problemDao.queryUserProblem(user.getUserId());
        for(int i=0;i<temp.size();i++){
            temp.get(i).setAnswerNum(answerDao.getCount(temp.get(i).getProblemId()));
        }
        return temp;
    }

    @Override
    public int getCount(User user) {
        if(StringsUtils.isNullOrEmpty(user.getUserId())){
            return 0;
        }
        return problemDao.getCount(user.getUserId());
    }
}