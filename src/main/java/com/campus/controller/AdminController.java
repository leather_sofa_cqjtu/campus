package com.campus.controller;

import com.campus.entity.Admin;
import com.campus.entity.User;
import com.campus.service.AdminService;
import com.campus.service.GoodsService;
import com.campus.service.UserService;
import com.campus.utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private UserService userService;

    @Autowired
    private GoodsService goodsService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public Map<String, Object> login(Admin admin) {
        Map<String, Object> modelMap = new HashMap<>();
        try {
            String token = adminService.login(admin);
            modelMap.put("status", true);
            modelMap.put("token", token);
        } catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }
        return modelMap;
    }

    //查询所有用户
    @RequestMapping(value = "/queryuser", method = RequestMethod.POST)
    public Map<String, Object> queryUser(String token, int pageNum) {
        Map<String, Object> modelMap = new HashMap<>();
        try {
            Admin admin = JwtUtil.unsign(token, Admin.class);
            if (adminService.isExist(admin)) {
                modelMap.put("status", true);
                modelMap.put("data", userService.queryUser(pageNum));
            } else {
                modelMap.put("status", false);
                modelMap.put("errMsg", "token认证失败");
            }
        } catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }
        return modelMap;
    }

    //冻结用户
    @RequestMapping(value = "frozenuser", method = RequestMethod.POST)
    public Map<String, Object> frozenUser(String token, String userId) {
        Map<String, Object> modelMap = new HashMap<>();
        try {
            Admin admin = JwtUtil.unsign(token, Admin.class);
            if (adminService.isExist(admin)) {
                if (userService.frozenUser(userId)) {
                    modelMap.put("status", true);
                    modelMap.put("data", "冻结成功!");
                }
            } else {
                modelMap.put("status", false);
                modelMap.put("errMsg", "token认证失败!");
            }
        } catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }

        return modelMap;
    }

    //解除冻结用户
    @RequestMapping(value = "unfrozenuser", method = RequestMethod.POST)
    public Map<String, Object> unFrozenUser(String token, String userId) {
        Map<String, Object> modelMap = new HashMap<>();
        try {
            Admin admin = JwtUtil.unsign(token, Admin.class);
            if (adminService.isExist(admin)) {
                if (userService.unFrozenUser(userId)) {
                    modelMap.put("status", true);
                    modelMap.put("data", "解除冻结成功!");
                }
            } else {
                modelMap.put("status", false);
                modelMap.put("errMsg", "token认证失败!");
            }
        } catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }

        return modelMap;
    }

    //商品审核不通过
    @RequestMapping(value = "/frozengoods", method = RequestMethod.POST)
    public Map<String, Object> frozenGoods(String token, int goodsId) {
        Map<String, Object> modelMap = new HashMap<>();
        try {
            Admin admin = JwtUtil.unsign(token,Admin.class);
            if (adminService.isExist(admin)) {
                if (goodsService.setCheckFlag(goodsId)) {
                    modelMap.put("status", true);
                    modelMap.put("data", "删除成功!");
                }else{
                    modelMap.put("status", false);
                    modelMap.put("errMsg", "删除失败!");
                }
            } else {
                modelMap.put("status", false);
                modelMap.put("errMsg", "token认证失败!");
            }
        } catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }
        return modelMap;
    }
}
