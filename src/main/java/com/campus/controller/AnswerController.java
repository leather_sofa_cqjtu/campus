package com.campus.controller;

import com.campus.entity.Answer;
import com.campus.entity.Problem;
import com.campus.entity.User;
import com.campus.service.AnswerService;
import com.campus.service.ProblemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/*
 * 类  名   :AnswerController
 * 描  述   :问答社区的回答接口
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-21
 * 说  明   :前后端绑定
 */
@RestController
@RequestMapping("/user")
public class AnswerController {

    @Autowired
    private AnswerService answerService;
    @Autowired
    private ProblemService problemService;
    //添加回答
    @RequestMapping(value = "/addAnswer",method = RequestMethod.POST)
    public Map<String,Object> addAnswer(Answer answer){
        Map<String, Object> modelMap =new HashMap<>();
        try {
            if(answerService.addAnswer(answer)){
                modelMap.put("status",true);
                modelMap.put("errMsg","回答成功");
            }
            else{
                modelMap.put("status", false);
                modelMap.put("errMsg", "回答失败,可能网络出小差了");
            }
        }catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }
        return modelMap;
    }
    //返回当前问题的所有回答
    @RequestMapping(value = "/queryAnswer",method = RequestMethod.POST)
    public Map<String,Object> queryAnswer(Problem problem){
        Map<String,Object> modelMap = new HashMap<>();
        modelMap.put("problemdetail",problemService.getProblem(problem));
        modelMap.put("answerList",answerService.queryAnswer(problem));
        return modelMap;
    }
    //返回当前用户的所有回答
    @RequestMapping(value = "queryUserAnswer",method = RequestMethod.POST)
    public Map<String,Object> queryUserAnswer(User user){
        Map<String,Object> modelMap = new HashMap<>();
        modelMap.put("answerUserList",answerService.queryUserAnswer(user));
        return modelMap;
    }
    //删除回复
    @RequestMapping(value = "/delAnswer",method = RequestMethod.POST)
    public Map<String,Object> delAnswer(Answer answer){
        Map<String,Object> modelMap = new HashMap<>();
        try {
            if(answerService.delAnwer(answer)){
                modelMap.put("status",true);
                modelMap.put("errMsg","删除回复成功");
            }else{
                modelMap.put("status",false);
                modelMap.put("errMsg","删除回复失败，可能网络出小差了");
            }
        }catch (Exception e){
            modelMap.put("status",false);
            modelMap.put("errMsg",e.getMessage());
        }
        return modelMap;
    }
    //查看回复数量
    @RequestMapping(value = "/getAnswerCount",method = RequestMethod.POST)
    public Map<String,Object> getCommentCount(Problem problem){
        Map<String,Object>modelMap = new HashMap<>();
        modelMap.put("count",answerService.getCount(problem));
        return modelMap;
    }
}
