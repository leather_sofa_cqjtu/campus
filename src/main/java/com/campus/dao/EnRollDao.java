package com.campus.dao;

import com.campus.entity.EnRoll;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/*
 * 接口名   :EnRollDao
 * 描  述   :活动申请数据访问对象
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-29
 * 说  明   :mybatis绑定
 */
public interface EnRollDao {
    //增加报名
    int addEnRoll(EnRoll enRoll);

    //删除报名
    int delEnRoll(EnRoll enRoll);

    //修改报名
    //int setEnRoll(EnRoll enRoll);

    //查看报名
    EnRoll getEnRoll(int enRollId);

    //罗列活动报名
    List<EnRoll> queryEnRoll(int activityId);

    //罗列用户报名
    List<EnRoll> queryUserEnRoll(String userId);

    //获取数量
    int getCount(String userId);

    //判断是否报名
            EnRoll isExist(@Param("activityId") int activityId,@Param("userId") String userId);
}
