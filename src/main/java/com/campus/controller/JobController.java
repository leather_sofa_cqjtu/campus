package com.campus.controller;

import com.campus.entity.Job;
import com.campus.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/job")
public class JobController {

    @Autowired
    private JobService jobService;

    @RequestMapping(value = "/queryJob", method = RequestMethod.POST)
    public Map<String, Object> queryJob(){
        Map<String, Object> modelMap = new HashMap<>();
        modelMap.put("job",jobService.queryJob());
        return modelMap;
    }
    @RequestMapping(value = "/addJob", method = RequestMethod.POST)
    public Map<String, Object> addLost(Job job){
        Map<String, Object> modelMap = new HashMap<>();
        try {
            int i=jobService.addJob(job);
            if(i>0)
                modelMap.put("state","true");
            else
                modelMap.put("state","false");
        }catch (Exception e){
            modelMap.put("state",e.getMessage());
        }
        return modelMap;
    }

}
