package com.campus.service;



import com.campus.entity.Activity;
import com.campus.entity.User;

import java.util.List;

public interface ActivityService {
    //添加活动
    boolean addActivity(Activity activity);
    //删除活动
    boolean delActivity(Activity activity);
    //查询活动
    Activity getActivity(int  activityId);
    //修改活动
    boolean setActivity(Activity activity);
    //展示全部活动
    List<Activity> queryActivity();
    //展示用户活动
    List<Activity> queryUserActivity(User user);
    //返回用户活动数量
    int getCount(User user);

    List<Activity> searchActivity(String key);
}
