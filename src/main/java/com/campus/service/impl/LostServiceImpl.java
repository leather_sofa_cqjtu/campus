package com.campus.service.impl;
import com.campus.dao.LostDao;
import com.campus.entity.Lost;
import com.campus.service.LostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class LostServiceImpl implements LostService {
    @Autowired
    private LostDao lostDao;

    @Override
    public List<Lost> queryLost(int page)
    {
        return lostDao.queryLost(page);
    }
    @Override
    public List<Lost> queryLostByUserId(String userId){
        return lostDao.queryLostByUserId(userId);
    }
    @Override
    public Lost queryLostById(Lost lost)
    {
        return lostDao.queryLostById(lost);
    }
    @Override
    public int addLost(Lost lost){
        lost.setCreateTime(new Date());
        return lostDao.addLost(lost);
    }
    @Override
    public int updateLostUser(Lost lost)
    {
        return lostDao.updateLostUser(lost);
    }
    @Override
    public int updateGetFlag(int lostId){
        return lostDao.updateGetFlag(lostId);
    }
}
