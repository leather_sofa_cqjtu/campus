package com.campus.service.impl;

import com.campus.dao.AnswerDao;
import com.campus.entity.Answer;
import com.campus.entity.Problem;
import com.campus.entity.User;
import com.campus.service.AnswerService;
import com.campus.utils.StringsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/*
 * 类  名   :AnswerServiceImpl
 * 描  述   :问答社区的回复服务逻辑处理类
 * 语  言   :
 * 作  者   :范泽华
 * 修  改   :
 * 日  期   :2018-06-21
 * 说  明   :逻辑处理
 */

@Service
public class AnswerServiceImpl implements AnswerService {
    @Autowired
    private AnswerDao answerDao;
    @Override
    public boolean addAnswer(Answer answer){
        try {
            if(StringsUtils.isNullOrEmpty(answer.getUser().getUserId())){
                return false;
            }
            answer.setCreateTime(new Date());
            int result = answerDao.addAnswer(answer);
            return result>0;
        }catch (Exception e) {
            throw  new RuntimeException("userId不存在");
        }
    }
    @Transactional
    @Override
    public boolean delAnwer(Answer answer) {
        try {
            if (StringsUtils.isNullOrEmpty(answer.getUser().getUserId())){
                return false;
            }
            int result = answerDao.delComment(answer);
            result += answerDao.delAnswer(answer);
            return result>0;
        }catch (Exception e){
            throw new RuntimeException("userId不存在|删除失败");
        }

    }

    @Override
    public Answer getAnswer(Answer answer) {
        return answerDao.getAnswer(answer.getAnswerId());
    }

    @Override
    public List<Answer> queryAnswer(Problem problem) {
        if(StringsUtils.isNullOrEmpty(problem.getProblemId().toString())){
            return null;
        }
        return answerDao.queryAnswer(problem.getProblemId());
    }

    @Override
    public List<Answer> queryUserAnswer(User user) {
        if (StringsUtils.isNullOrEmpty(user.getUserId())){
            return null;
        }
        return answerDao.queryUserAnswer(user.getUserId());
    }

    @Override
    public int getCount(Problem problem) {
        if(StringsUtils.isNullOrEmpty(problem.getProblemId().toString())) {
            return 0;
        }
        return answerDao.getCount(problem.getProblemId());
    }
}
