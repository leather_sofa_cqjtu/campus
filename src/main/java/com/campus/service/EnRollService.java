package com.campus.service;



import com.campus.entity.Activity;
import com.campus.entity.EnRoll;
import com.campus.entity.User;

import java.util.List;

public interface EnRollService {
    //添加活动报名
    boolean addEnRoll(EnRoll enRoll);
    //删除活动报名
    boolean delEnRoll(EnRoll enRoll);
    //查询活动报名
    EnRoll getEnRoll(EnRoll enRoll);
    //展示全部活动报名
    List<EnRoll> queryEnRoll(Activity activity);
    //展示用户活动报名
    List<EnRoll> queryUserEnRoll(User user);
    //返回用户活动报名数量
    int getCount(User user);

    boolean isExist(int activityId,String userId);
}
