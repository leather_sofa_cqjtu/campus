package com.campus.controller;

import com.campus.entity.Activity;
import com.campus.entity.EnRoll;
import com.campus.entity.User;
import com.campus.service.EnRollService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/user")
public class EnRollController {
    @Autowired
    private EnRollService enRollService;

    //增加报名
    @RequestMapping(value = "addEnRoll",method = RequestMethod.POST)
    public Map<String,Object> addEnRoll(EnRoll enRoll){
        Map<String,Object> modelMap = new HashMap<>();
        try {
            if(enRollService.addEnRoll(enRoll)){
                modelMap.put("status",true);
                modelMap.put("errMsg","报名成功");
            }
            else{
                modelMap.put("status", false);
                modelMap.put("errMsg", "报名失败,可能网络出小差了");
            }
        }catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }
        return modelMap;
    }

    //删除报名
    @RequestMapping(value = "dekEnRoll",method = RequestMethod.POST)
    public Map<String,Object> delEnRoll(EnRoll enRoll){
        Map<String,Object> modelMap = new HashMap<>();
        try {
            if(enRollService.delEnRoll(enRoll)){
                modelMap.put("status",true);
                modelMap.put("errMsg","删除报名成功");
            }
            else{
                modelMap.put("status", false);
                modelMap.put("errMsg", "删除报名失败,可能网络出小差了");
            }
        }catch (Exception e) {
            modelMap.put("status", false);
            modelMap.put("errMsg", e.getMessage());
        }
        return modelMap;
    }

    //罗列活动报名
    @RequestMapping(value = "queryEnRoll",method = RequestMethod.POST)
    public Map<String,Object> queryEnRoll(Activity activity){
        Map<String,Object> modelMap = new HashMap<>();
        modelMap.put("enRollList",enRollService.queryEnRoll(activity));
        return modelMap;
    }
    //罗列用户的报名
    @RequestMapping(value = "queryUserEnRoll",method = RequestMethod.POST)
    public Map<String,Object> queryUserEnRoll(User user){
        Map<String,Object> modelMap = new HashMap<>();
        modelMap.put("enRollUserList",enRollService.queryUserEnRoll(user));
        return modelMap;
    }
    //返回用户报名数量
    @RequestMapping(value = "getUserEnRollCount",method = RequestMethod.POST)
    public Map<String,Object> getUserEnRollCount(User user){
        Map<String,Object> modelMap = new HashMap<>();
        modelMap.put("count",enRollService.getCount(user));
        return modelMap;
    }
}
